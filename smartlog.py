# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from threading import Thread
import traceback
import requests
import zipfile
import logging
import string
import random
import time
import gzip
import sys
import six

from django.conf import settings
from django.db import transaction
from django.utils import timezone


class SmartLog(object):
    '''
    Usage: logger = SmartLog.getLogger(name)
    '''

    log_queue1 = []
    log_queue2 = []
    log_queue3 = []
    log_queue4 = []
    log_queue5 = []
    log_queue6 = []
    log_queue7 = []
    log_queue8 = []
    log_queue9 = []
    log_queue10 = []

    log_queue11 = []
    log_queue12 = []
    log_queue13 = []
    log_queue14 = []
    log_queue15 = []
    log_queue16 = []
    log_queue17 = []
    log_queue18 = []
    log_queue19 = []
    log_queue20 = []

    log_thread1 = None
    log_thread2 = None
    log_thread3 = None
    log_thread4 = None
    log_thread5 = None
    log_thread6 = None
    log_thread7 = None
    log_thread8 = None
    log_thread9 = None
    log_thread10 = None

    log_thread11 = None
    log_thread12 = None
    log_thread13 = None
    log_thread14 = None
    log_thread15 = None
    log_thread16 = None
    log_thread17 = None
    log_thread18 = None
    log_thread19 = None
    log_thread20 = None


    session = None

    def __init__(self, logger, tags=None, content_object=None, uid=None, name=None, compress=True):
        self.logger           = logger
        self.name             = name if name else self.logger.name
        self.tags             = tags
        self.content_object   = content_object
        self.buffer_message   = ''
        self.parent_log       = None
        self.buffer_log       = []
        self.buffer_log_limit = 20
        self.zenlog_url       = getattr(settings, 'ZENLOG_URL', False)
        self.compress         = compress
        
        if not self.session:
            self.session = requests.session()
            
        if uid:
            self._uid = uid
        self.uid  # just call to create uid

    def __repr__(self):
        if hasattr(self, 'logger'):
            return '<SmartLog:%s_%s>' % (self.name, self.uid)
        else:
            return super(SmartLog, self).__repr__()

    @property
    def uid(self):
        if not hasattr(self, '_uid'):
            self._uid = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(32))
        return self._uid

    def add_tag(self, tag):
        new_tags = self.tags
        if new_tags:
            new_tags += ',' + tag
        else:
            new_tags = tag
        new_log = self.__class__(self.logger, new_tags, self.content_object, self.uid, self.name)
        new_log.parent_log = self
        return new_log

    def _pseudo_log(self, entity, message):
        if message:
            buffer_message = self.buffer_message
            try:
                message = str(message)
            except (UnicodeDecodeError, UnicodeEncodeError):
                message = message.encode('utf-8').decode('utf-8')  # Verify this is unicode str!
                # message = str(message, 'unicode_escape')
            buffer_message += message + '\n'
            BUFFER_LIMIT = 1024 * 512
            BUFFER_FLUSH = int(BUFFER_LIMIT // 8)
            if len(self.buffer_message) > BUFFER_LIMIT:  # buffer log more than 0.5MB
                buffer_message = buffer_message[0:65536:1]  # get tail 0.08MB
            self.buffer_message = buffer_message
        if self.parent_log:  # update parent too
            self.parent_log._pseudo_log(entity, message)
        else:  # this is root
            self.buffer_log.append(entity)

    def async_log(self, entity):

        def _async_log(log_queue):
            ''' Merge tag '''
            while log_queue:
                entity = log_queue.pop(0)
                tags = entity['tags']

                data = {
                    'project_name': entity.get('name'),
                    'message': entity['message'],
                    'tags_name': tags,
                    'group_uid': entity.get('group_uid'),
                    'level': entity['level'],
                    'created_on': entity.get('created_on'),
                }
                files = {}
                if entity['attachment']:
                    if self.compress:
                        try:
                            try:
                                from StringIO import StringIO # Python 2.7
                                ClassIO = StringIO
                            except:
                                # from io import StringIO  # Python 3
                                from io import BytesIO
                                ClassIO = BytesIO

                            s = ClassIO()
                            with gzip.GzipFile('attachment.txt', mode='wb', fileobj=s) as z:
                                if not isinstance(entity['attachment'], bytes):  # if unicode
                                    entity['attachment'] = entity['attachment'].encode('utf-8')
                                z.write(entity['attachment'])
                            compressed = s.getvalue()
                            s.close()
                        except ImportError:  # Python 3
                            if type(entity['attachment']) != bytes:
                                data_encoding = bytes(entity['attachment'], 'utf-8')
                            else:
                                data_encoding = entity['attachment']
                            compressed = gzip.compress(data_encoding)
                        files = {
                            'attachment': ('attachment.gz', compressed)
                        }
                    else:
                        files = {
                            'attachment': ('attachment.raw', entity['attachment'])
                        }
                try:
                    self.session.post(self.zenlog_url, data=data, files=files, timeout=10)
                except requests.exceptions.RequestException:
                    print('Warning: Cannot send log to server')
                time.sleep(0.2)

        def start_thread(log_queue, log_thread):
            log_queue.append(entity)
            if log_thread and log_thread.is_alive():
                return
            log_thread = Thread(target=_async_log, args=(log_queue,))
            log_thread.start()
            
        if self.zenlog_url:
            ###### เพื่อป้องกันลำดับของ log ผิดเพี้ยน จึงใช้วิธีรวม log ที่มีเวลาเดียว อยู่ใน queue เดียวกัน #####
            key = entity.get('created_on', timezone.now()).second % 20
            key = str(key+1)

            if key=="1":
                SmartLog.log_queue1.append(entity)
                if SmartLog.log_thread1 and SmartLog.log_thread1.is_alive():
                    return
                SmartLog.log_thread1 = Thread(target=_async_log, args=(SmartLog.log_queue1,))
                SmartLog.log_thread1.start()
            elif key=="2":
                SmartLog.log_queue2.append(entity)
                if SmartLog.log_thread2 and SmartLog.log_thread2.is_alive():
                    return
                SmartLog.log_thread2 = Thread(target=_async_log, args=(SmartLog.log_queue2,))
                SmartLog.log_thread2.start()
            elif key=="3":
                SmartLog.log_queue3.append(entity)
                if SmartLog.log_thread3 and SmartLog.log_thread3.is_alive():
                    return
                SmartLog.log_thread3 = Thread(target=_async_log, args=(SmartLog.log_queue3,))
                SmartLog.log_thread3.start()
            elif key=="4":
                SmartLog.log_queue4.append(entity)
                if SmartLog.log_thread4 and SmartLog.log_thread4.is_alive():
                    return
                SmartLog.log_thread4 = Thread(target=_async_log, args=(SmartLog.log_queue4,))
                SmartLog.log_thread4.start()
            elif key=="5":
                SmartLog.log_queue5.append(entity)
                if SmartLog.log_thread5 and SmartLog.log_thread5.is_alive():
                    return
                SmartLog.log_thread5 = Thread(target=_async_log, args=(SmartLog.log_queue5,))
                SmartLog.log_thread5.start()
            elif key=="6":
                SmartLog.log_queue6.append(entity)
                if SmartLog.log_thread6 and SmartLog.log_thread6.is_alive():
                    return
                SmartLog.log_thread6 = Thread(target=_async_log, args=(SmartLog.log_queue6,))
                SmartLog.log_thread6.start()
            elif key=="7":
                SmartLog.log_queue7.append(entity)
                if SmartLog.log_thread7 and SmartLog.log_thread7.is_alive():
                    return
                SmartLog.log_thread7 = Thread(target=_async_log, args=(SmartLog.log_queue7,))
                SmartLog.log_thread7.start()
            elif key=="8":
                SmartLog.log_queue8.append(entity)
                if SmartLog.log_thread8 and SmartLog.log_thread8.is_alive():
                    return
                SmartLog.log_thread8 = Thread(target=_async_log, args=(SmartLog.log_queue8,))
                SmartLog.log_thread8.start()
            elif key=="9":
                SmartLog.log_queue9.append(entity)
                if SmartLog.log_thread9 and SmartLog.log_thread9.is_alive():
                    return
                SmartLog.log_thread9 = Thread(target=_async_log, args=(SmartLog.log_queue9,))
                SmartLog.log_thread9.start()
            elif key=="10":
                SmartLog.log_queue10.append(entity)
                if SmartLog.log_thread10 and SmartLog.log_thread10.is_alive():
                    return
                SmartLog.log_thread10 = Thread(target=_async_log, args=(SmartLog.log_queue10,))
                SmartLog.log_thread10.start()

            elif key=="11":
                SmartLog.log_queue11.append(entity)
                if SmartLog.log_thread11 and SmartLog.log_thread11.is_alive():
                    return
                SmartLog.log_thread11 = Thread(target=_async_log, args=(SmartLog.log_queue11,))
                SmartLog.log_thread11.start()
            elif key=="12":
                SmartLog.log_queue12.append(entity)
                if SmartLog.log_thread12 and SmartLog.log_thread12.is_alive():
                    return
                SmartLog.log_thread12 = Thread(target=_async_log, args=(SmartLog.log_queue12,))
                SmartLog.log_thread12.start()
            elif key=="13":
                SmartLog.log_queue13.append(entity)
                if SmartLog.log_thread13 and SmartLog.log_thread13.is_alive():
                    return
                SmartLog.log_thread13 = Thread(target=_async_log, args=(SmartLog.log_queue13,))
                SmartLog.log_thread13.start()
            elif key=="14":
                SmartLog.log_queue14.append(entity)
                if SmartLog.log_thread14 and SmartLog.log_thread14.is_alive():
                    return
                SmartLog.log_thread14 = Thread(target=_async_log, args=(SmartLog.log_queue14,))
                SmartLog.log_thread14.start()
            elif key=="15":
                SmartLog.log_queue15.append(entity)
                if SmartLog.log_thread15 and SmartLog.log_thread15.is_alive():
                    return
                SmartLog.log_thread15 = Thread(target=_async_log, args=(SmartLog.log_queue15,))
                SmartLog.log_thread15.start()
            elif key=="16":
                SmartLog.log_queue16.append(entity)
                if SmartLog.log_thread16 and SmartLog.log_thread16.is_alive():
                    return
                SmartLog.log_thread16 = Thread(target=_async_log, args=(SmartLog.log_queue16,))
                SmartLog.log_thread16.start()
            elif key=="17":
                SmartLog.log_queue17.append(entity)
                if SmartLog.log_thread17 and SmartLog.log_thread17.is_alive():
                    return
                SmartLog.log_thread17 = Thread(target=_async_log, args=(SmartLog.log_queue17,))
                SmartLog.log_thread17.start()
            elif key=="18":
                SmartLog.log_queue18.append(entity)
                if SmartLog.log_thread18 and SmartLog.log_thread18.is_alive():
                    return
                SmartLog.log_thread18 = Thread(target=_async_log, args=(SmartLog.log_queue18,))
                SmartLog.log_thread18.start()
            elif key=="19":
                SmartLog.log_queue19.append(entity)
                if SmartLog.log_thread19 and SmartLog.log_thread19.is_alive():
                    return
                SmartLog.log_thread19 = Thread(target=_async_log, args=(SmartLog.log_queue19,))
                SmartLog.log_thread19.start()
            elif key=="20":
                SmartLog.log_queue20.append(entity)
                if SmartLog.log_thread20 and SmartLog.log_thread20.is_alive():
                    return
                SmartLog.log_thread20 = Thread(target=_async_log, args=(SmartLog.log_queue20,))
                SmartLog.log_thread20.start()

    def _log(self, msg=None, attachment=None, content_object=None, level='DEBUG'):
        if attachment:
            attachment_size = len(attachment)
            if attachment_size > 2 * 1024 * 1024:  # More than 2MB
                attachment = 'AttachmentWarning: Too large %.2fMB' % (attachment_size / (1024 * 1024))

        tags = self.tags
        if tags:
            if content_object:
                try:
                    tags += ',' + str(content_object)
                except:
                    pass
        else:
            if content_object:
                try:
                    tags = str(content_object)
                except:
                    pass
        entity = dict(message=msg, attachment=attachment, tags=tags, created_on=timezone.now(), level=level, group_uid=self.uid, name=self.name)
        self.async_log(entity)
        self._pseudo_log(entity, msg)

    def verbose(self, msg=None, attachment=None, content_object=None):
        '''
        Verbose level did not send to zenlog server.
        '''
        if msg:
            self.logger.debug(msg)

    def debug(self, msg=None, attachment=None, content_object=None):
        if msg:
            self.logger.debug(msg)
        self._log(msg, attachment, content_object, 'DEBUG')

    def info(self, msg=None, attachment=None, content_object=None):
        if msg:
            self.logger.info(msg)
        self._log(msg, attachment, content_object, 'INFO')

    def warning(self, msg=None, attachment=None, content_object=None):
        if msg:
            self.logger.warning(msg)
        self._log(msg, attachment, content_object, 'WARNING')

    def error(self, msg=None, attachment=None, content_object=None):
        if msg:
            extra = {
                'stack': True,
            }
            self.logger.error(msg, extra=extra)
        self._log(msg, attachment, content_object, 'ERROR')

    def critical(self, msg=None, attachment=None, content_object=None):
        if msg:
            self.logger.critical(msg)
        self._log(msg, attachment, content_object, 'CRITICAL')
        # self.save_error()

    def flush(self):
        self.buffer_message = ''

    @staticmethod
    def getLogger(name, uid=None):
        logger = logging.getLogger(name)
        return SmartLog(logger, name=name, uid=uid)

    @property
    def message(self):
        return self.buffer_message

    # def save_error(self):
    #     raise Exception('DeprecateError: save_error()')

    def __del__(self):
        pass
        # if not self.parent_log:
        #     self.save_error()
