# -*- coding: utf-8 -*-

from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from smartlog.smartlog import SmartLog


@login_required
def index(request):
    html = ''
    
    for index, q in enumerate(SmartLog.log_queues):
        html += '</br> log_queues index = {}, inqueue = {}, thread = {}'.format(index, len(q), str(SmartLog.log_threads[index]))

    return HttpResponse(html)