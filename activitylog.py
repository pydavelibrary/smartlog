# -*- coding: utf-8 -*-

from threading import Thread
import requests

from django.conf import settings


class ActivityLog(object):
    """docstring for ActivityLog"""

    _logs = {}

    def __init__(self, name):
        super(ActivityLog, self).__init__()
        self.name = name
        self.url = getattr(settings, 'ACTIVITYLOG_URL', None)

    def log(self, message, tags=None, user=None):

        def _async_log(data):
            try:
                requests.post(self.url, data=data, timeout=5)
            except requests.exceptions.RequestException:
                print('Warning: Cannot send log to server')

        if self.url:
            tags_name = ''
            if isinstance(tags, dict):
                tags_name = ','.join(['%s:%s' % (x, y) for x, y in list(tags.items())])
            elif isinstance(tags, list):
                tags_name = ','.join(tags)
            else:
                tags_name = str(tags)
            data = {
                'project_name': self.name,
                'message': message,
                'tags_name': tags_name,
                'user_username': user.username if user else 'guest',
                'user_nickname': user.username if user else None,
            }
            if getattr(settings, 'USE_THREAD_LOG', True):
                thread = Thread(target=_async_log, args=(data,))
                thread.start()
            else:
                _async_log(data)

    @staticmethod
    def getLogger(name):
        if name not in ActivityLog._logs:
            ActivityLog._logs[name] = ActivityLog(name)
        return ActivityLog._logs[name]
